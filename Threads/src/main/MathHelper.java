package main;

import java.math.BigInteger;

public class MathHelper implements Runnable {
	
	private long number;
	
	public MathHelper() {}
	
	public MathHelper(long number) {
		this.number = number;
	}
	
	public boolean isPrime() {
		for(int i = 2; i <= Math.sqrt(this.number); i++) {
			if(this.number % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isPrimeWhile() {
		int i = 2;
		while(i < this.number) {
			if(this.number % i == 0) {
				return false;
			}
			i++;
		}
		return true;
	}
	
	public boolean isPrimeBI(BigInteger n) {
		BigInteger i = new BigInteger("2");
		BigInteger one = new BigInteger("1");
		while(true) {
			if(n.mod(i).equals(new BigInteger("0"))) {
				return false;
			}
			i = i.add(one);
			//System.out.println(i.compareTo(n), i, n);
			if(i.compareTo(n) == 0) {
				break;
			}
		}
		return true;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		BigInteger ip = this.sumToBI(new BigInteger("" + this.number));//this.isPrimeBI(new BigInteger(this.number + ""));
		long stop = System.currentTimeMillis();
		System.out.println("Suma liczb od 1 do " + this.number + " wynosi: " 
								+ ip + " (wykonanie zajelo "+ convertTime(stop - start) + ")");
	}
	
	private String convertTime(long time) {
		long s = time / 1000;
		long min = 0;
		String ret = "";
		if(s > 60) {
			min = (long) Math.floor(s / 60);
			s = s - min * 60;
		}
		if(min > 0) ret += min + "min ";
		if(s > 0) ret += s + "sec";
		else ret += time + "ms";
		return ret;
	}
	
	public String convertTime(BigInteger time) {
		return "";
	}
	
	public BigInteger sumToBI(BigInteger n) {
		/* metoda zwracajaca sume 1...n */
		BigInteger bi = new BigInteger("1");
		BigInteger i = new BigInteger("0");
		while(n.compareTo(bi) != 0) {
			bi = bi.add(BigInteger.ONE);
		}
		return bi;
	}
}
