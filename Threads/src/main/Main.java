package main;

import java.math.BigInteger;
import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		/*
		Thread t = Thread.currentThread();
		System.out.println( t.getName() );
		Thread t1, t2;
		//Thread t2 = new Thread(new MathHelper(3), "moj watek");
		//t2.start(); // uwazamy na uzycie metody start(), gdyz niepoprawnie uzyta metoda run() nie wystartuje watku
		LinkedList<BigInteger> primes = new LinkedList<>();
		
		primes.add(new BigInteger("472882049"));
		primes.add(new BigInteger("32452867"));
		primes.add(new BigInteger("941083987"));
		primes.add(new BigInteger("3"));
		
		for(int i = 0; i < primes.size(); i += 2) {
			BigInteger l1 = primes.get(i);
			BigInteger l2 = primes.get(i + 1);
			t1 = new Thread(new MathHelper(l1.longValue()), "Watek " + i);
			t2 = new Thread(new MathHelper(l2.longValue()), "Watek " + (i + 1));
			t1.setPriority(Thread.MAX_PRIORITY);
			t1.start();
			t2.start();
		}
		System.out.println("KONIEC");
		*/
		
		
		/*BigInteger bi = new BigInteger("9400000000000000000000000001083987");
		bi = bi.add(new BigInteger("5"));
		bi = bi.mod()
		System.out.println(bi);*/
		
		Thread t = new Thread(new MathHelper(1231233L), "SumujacyWatek");
		t.start();
		
	}

}
