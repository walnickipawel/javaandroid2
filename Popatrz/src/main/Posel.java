package main;

public class Posel {
	/*"sejm_kluby.nazwa": "Platforma Obywatelska",
    "poslowie.id": "1168",
    "poslowie.klub_id": "1",
    "poslowie.nazwa": "Jan Kulas",*/
	
	private String nazwaKlubu;
	private String nazwa;
	
	public Posel() {}

	public String getNazwaKlubu() {
		return nazwaKlubu;
	}

	public void setNazwaKlubu(String nazwaKlubu) {
		this.nazwaKlubu = nazwaKlubu;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Posel(String nazwaKlubu, String nazwa) {
		this.nazwaKlubu = nazwaKlubu;
		this.nazwa = nazwa;
	}
	
	
	
	
}
