package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
/* https://randomuser.me/api/?nat=US */
public class HTTPConnector {
	private String url;
	private URL obj;
	private HttpURLConnection con;
	private String ua;
	
	public HTTPConnector() {
		
	}
	
	public String sendGET(String url) throws IOException {
		/* definiujemy adres polaczenia (hosta) */
		/* definiujemy przegladarke (user-agent) */
		String ua = "Pawel/1.0";
		/* definiujemy nowy obiekt typu URL */
		URL obj = new URL(url);
		/* definiujemy polaczenie */
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		/* opcjonalnie */
		con.setRequestMethod("GET");

		/* definiujemy naglowek - dodajemy user-agenta */
		con.setRequestProperty("User-Agent", ua);

		/* pobieramy kod zwrotki (http response code) */
		int responseCode = con.getResponseCode();
		/* definiujemy zmienne dla aktualnej lini oraz calego pliku */
		String ret = "", currentLine = "";
		if (responseCode == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while((currentLine = br.readLine()) != null) {
				ret += currentLine;
			}
			/* zamykamy readera */
			br.close();
		} else {
			System.out.println("Code: " + responseCode);
		}
		//System.out.println(ret);
		/* zwracamy odpowiedz */
		return ret;
	}
}
