package main;

import java.io.IOException;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MojePanstwo {
	private final String apiURL = "https://api-v3.mojepanstwo.pl/dane/poslowie_oswiadczenia_majatkowe";
	
	
	public MojePanstwo() {
		
	}
	
	public void showData() throws IOException, ParseException {
		HTTPConnector hc = new HTTPConnector();
		String myJSON = hc.sendGET(this.apiURL);
		JSONParser jp = new JSONParser();
		
		JSONObject jo = (JSONObject) jp.parse(myJSON);
		
		JSONArray ja = (JSONArray) jo.get("Dataobject");
		
		
		for(int i = 0; i < 10; i++) {
			JSONObject joo = (JSONObject) ja.get(i);
			JSONObject jooo = (JSONObject) joo.get("data");
			Posel posel = new Posel();
			posel.setNazwa(jooo.get("poslowie.nazwa").toString());
			posel.setNazwaKlubu(jooo.get("sejm_kluby.nazwa").toString());
			System.out.println(posel.getNazwa() + "( " + posel.getNazwaKlubu() + " )");
		}
	}
}
